let userNumberFirst = prompt("Please, type number first");
if (!isValidNumber(userNumberFirst)) {
  userNumberFirst = +prompt("Please, type number first again");
}

let userNumberSecond = prompt("Please, type number second");
if (!isValidNumber(userNumberSecond)) {
  userNumberSecond = +prompt("Please, type number second again");
}

let userCalcOperation = prompt("Please, type the calc operation: +, -, *, /");

function calcResult(a, b, c) {
  switch (c) {
    case "+":
      return +a + +b;
    case "-":
      return a - b;
    case "*":
      return a * b;
    case "/":
      return a / b;
  }
}

console.log(calcResult(userNumberFirst, userNumberSecond, userCalcOperation));

function isValidNumber(value) {
  if (value === "" || value === null || Number.isNaN(+value)) {
    return false;
  }
  return true;
}
